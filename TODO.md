# TODO

## SOON-ish

* Move things around a little for the "/usr merge" project.

* Support cloud auto-detection, where it's possible to do so.

* Test improvements - each user-data handler should be tested with each cloud.

## FUTURE

* Support additional features of `#cloud-config` as needed

* Support LVM partitioning and non-`ext[234]` filesystems?

* Other cloud providers?
