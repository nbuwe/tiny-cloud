PREFIX ?= /

SUBPACKAGES = core openrc aws azure gcp hetzner incus nocloud oci scaleway

.PHONY: check install $(SUBPACKAGES)

install: $(SUBPACKAGES)

core:
	install -Dm755 -t "$(PREFIX)"/usr/bin \
		bin/imds
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud \
		lib/tiny-cloud/common \
		lib/tiny-cloud/init \
		lib/tiny-cloud/tiny-cloud.conf
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/user-data \
		lib/tiny-cloud/user-data/alpine-config \
		lib/tiny-cloud/user-data/missing \
		lib/tiny-cloud/user-data/script \
		lib/tiny-cloud/user-data/unknown
	install -Dm644 lib/tiny-cloud/tiny-cloud.conf \
		"$(PREFIX)"/etc/tiny-cloud.conf
	install -Dm755 -t "$(PREFIX)"/usr/sbin \
		sbin/tiny-cloud

openrc:
	install -Dm755 -t "$(PREFIX)"/etc/init.d \
		dist/openrc/*

aws:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/aws \
		lib/tiny-cloud/cloud/aws/*

azure:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/azure \
		lib/tiny-cloud/cloud/azure/*

gcp:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/gcp \
		lib/tiny-cloud/cloud/gcp/*

hetzner:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/hetzner \
		lib/tiny-cloud/cloud/hetzner/*

incus:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/incus \
		lib/tiny-cloud/cloud/incus/imds
	install -Dm755 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/incus \
		lib/tiny-cloud/cloud/incus/autodetect

nocloud:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/nocloud \
		lib/tiny-cloud/cloud/nocloud/init \
		lib/tiny-cloud/cloud/nocloud/imds
	install -Dm755 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/nocloud \
		lib/tiny-cloud/cloud/nocloud/autodetect

oci:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/oci \
		lib/tiny-cloud/cloud/oci/*

scaleway:
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/scaleway \
		lib/tiny-cloud/cloud/scaleway/*

check: tests/Kyuafile Kyuafile
	kyua --variable parallelism=$(shell nproc) test || (kyua report --verbose && exit 1)

tests/Kyuafile: $(wildcard tests/*.test)
	echo "syntax(2)" > $@.tmp
	echo "test_suite('tiny-cloud')" >> $@.tmp
	for i in $(notdir $(wildcard tests/*.test)); do \
		echo "atf_test_program{name='$$i',timeout=5}" >> $@.tmp ; \
	done
	mv $@.tmp $@

Kyuafile:
	echo "syntax(2)" > $@.tmp
	echo "test_suite('tiny-cloud')" >> $@.tmp
	echo "include('tests/Kyuafile')" >> $@.tmp
	mv $@.tmp $@
